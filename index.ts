import express, { json } from 'express';
import * as dotenv from 'dotenv';
import OpenAI from 'openai';
import { ChatCompletion, ChatCompletionMessageParam } from 'openai/resources/chat';
import { Assistants } from 'openai/resources/beta/assistants/assistants';


const app = express();
const port = 8080;
dotenv.config();


var openai = new OpenAI({apiKey :process.env.OPEN_API_KEY});

app.listen(port,()=>{
    console.log(`API listening on http://localhost:${port}`);
})

// Middleware to parse JSON
app.use(express.json());

// Middleware to parse URL-encoded data
app.use(express.urlencoded({ extended: true }));


var response :ChatCompletion|null=null;
var message:ChatCompletionMessageParam ;
var role: string = 'user';
app.post('/',async(req,res)=>{
    try {
      console.log('Before progress....');
      var requestdata:Record<string,string> = req.body;
      
      while(!response ){
          message = {role:'user',content: requestdata['message']};
      if (message !=null){
        console.log('In progress....');
      
        response = await openai.chat.completions.create({
          messages: [message], // Ensure the messages are in an array
          model: 'gpt-3.5-turbo',        }
        
        );
        res.json(response.choices[0].message.content);
        console.log('Done....');
      }
      else{console.log("double check your message")};
    }

     
    } catch (error:any) {
      console.error(error.message);
      res.status(500).send('Internal Server Error');
    }
      });

    

app.get('/', async (req, res) => {
    try {
      console.log('Nham conme no ham');
      req.body;
   
      // Response will be in that precise text, but you can explore the full object if you want to
      res.send(response);
    } catch (error: any) {
        if (error.response) {
            console.error(error.response.status);
            console.error(error.response.data);
        } else {
            console.error(error.message);
        }
    }
  }
  );