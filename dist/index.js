"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv = __importStar(require("dotenv"));
const openai_1 = __importDefault(require("openai"));
const app = (0, express_1.default)();
const port = 8080;
dotenv.config();
var openai = new openai_1.default({ apiKey: process.env.OPEN_API_KEY });
app.listen(port, () => {
    console.log(`API listening on http://localhost:${port}`);
});
// Middleware to parse JSON
app.use(express_1.default.json());
// Middleware to parse URL-encoded data
app.use(express_1.default.urlencoded({ extended: true }));
// OpenAI uses Axios
var response = null;
var message;
var role = 'user';
app.post('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        console.log('Before progress....');
        var requestdata = req.body;
        while (!response) {
            message = { role: 'user', content: requestdata['message'] };
            if (message != null) {
                console.log('In progress....');
                response = yield openai.chat.completions.create({
                    messages: [message], // Ensure the messages are in an array
                    model: 'gpt-3.5-turbo',
                });
                res.json(response.choices[0].message.content);
                console.log('Done....');
            }
            else {
                console.log("double check your message");
            }
            ;
        }
    }
    catch (error) {
        console.error(error.message);
        res.status(500).send('Internal Server Error');
    }
}));
app.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        console.log('Nham conme no ham');
        req.body;
        // Response will be in that precise text, but you can explore the full object if you want to
        res.send(response);
    }
    catch (error) {
        if (error.response) {
            console.error(error.response.status);
            console.error(error.response.data);
        }
        else {
            console.error(error.message);
        }
    }
}));
